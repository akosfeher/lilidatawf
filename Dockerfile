
FROM jboss/wildfly:14.0.1.Final

LABEL hu.itbt.version="1.1"
LABEL author="Akos Feher"
LABEL email="feher.akos@sgmail.com"
LABEL vendor1="IT Bt"
LABEL hu.itbt.release-date="2019-07-02"

# Set Postgresql env variables
ENV DB_HOST postgres
ENV DB_PORT 5432
ENV DB_NAME postgres
ENV POSTGRES_USER=postgres
ENV POSTGRES_PASSWORD=postgres
ENV DB_USER postgres
ENV DB_PASS postgres

ENV DS_NAME postgresDS
ENV JNDI_NAME java:/PostgresLilidataDS

ENV JAVA_OPTS -server -Xms512m -Xmx2048m -XX:MetaspaceSize=96M -XX:MaxMetaspaceSize=256m -XX:+UseAdaptiveSizePolicy -XX:MaxMetaspaceSize=1024m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true

USER root

ADD https://jdbc.postgresql.org/download/postgresql-42.2.4.jar /tmp/postgresql-42.2.4.jar

# RUN apt install postgresql-client-common
WORKDIR /tmp
COPY input_files/wildfly-command.sh ./
COPY input_files/module-install.cli ./
# COPY input_files/game4teamInit.sql ./
COPY input_files/default.properties ./
COPY input_files/StoredCredential_fa ./
COPY input_files/StoredCredential_54 ./
copy input_files/mysql-connector-java-8.0.20.jar ./

RUN sed -i -e 's/\r$//' ./wildfly-command.sh
RUN chmod +x ./wildfly-command.sh
RUN ./wildfly-command.sh \
    &&  rm -rf $JBOSS_HOME/standalone/configuration/standalone_xml_history/

# Download and deploy the war file
ADD target/lilidatawf.war $JBOSS_HOME/standalone/deployments
# ADD input_files/game4teamInit.sql $JBOSS_HOME/standalone/configuration
ADD input_files/default.properties $JBOSS_HOME/standalone/configuration
ADD input_files/StoredCredential_fa $JBOSS_HOME/standalone/configuration/tokens/
ADD input_files/StoredCredential_54 $JBOSS_HOME/standalone/configuration/tokens/

# Create Wildfly admin user
RUN $JBOSS_HOME/bin/add-user.sh admin admin --silent

# Set the default command to run on boot
# This will boot WildFly in the standalone mode and bind to all interface
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
