package hu.itbt.lilidata.service;

import com.google.gson.JsonObject;

public class Identifiable {

	protected JsonObject data;

	public boolean isUnidentified() {
		return (data == null || !data.has("id") || data.get("id") == null
				|| data.get("id").getAsBigInteger().longValue() < 1);
	}

	public JsonObject getData() {
		return data;
	}

	public Identifiable(JsonObject data) {
		this.data = data;
	}

	public Identifiable(Long id) {
		JsonObject jo = new JsonObject();
		jo.addProperty("id", id);
		this.data = jo;
	}

	public void setId(Long id) {
		this.data.addProperty("id", id);
	}

	public Long identity() {
		if (isUnidentified()) {
			return null;
		}
		return new Long(data.get("id").getAsBigInteger().longValue());
	}
}
