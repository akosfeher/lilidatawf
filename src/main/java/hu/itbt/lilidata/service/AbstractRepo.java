package hu.itbt.lilidata.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public abstract class AbstractRepo {

	private static String DATASOURCE_CONTEXT = "java:/PostgresLiliDS";

	private static final boolean cacheColumns = true;

	private static Map<String, Map<String, String>> tableColumns = new HashMap<>();

	private static Logger logger = Logger.getLogger(AbstractRepo.class.getName());

	protected AbstractRepo() {
		super();
	}

	protected void close(ResultSet aResultSet) {
		if (aResultSet != null) {
			try {
				aResultSet.close();
			} catch (Exception e) {
				// ignore
			}
		}
	}

	protected void close(Statement aStatement) {
		if (aStatement != null) {
			try {
				aStatement.close();
			} catch (Exception e) {
				// ignore
			}
		}
	}

	protected Connection connection() throws SQLException {
		return connection(DATASOURCE_CONTEXT);
	}

	protected Connection connection(String dsJNDI) throws SQLException {
		Connection result = null;
		try {
			Context initialContext = new InitialContext();
			// cast is necessary
			DataSource datasource = (DataSource) initialContext.lookup(dsJNDI == null ? DATASOURCE_CONTEXT : dsJNDI);
			if (datasource != null) {
				result = datasource.getConnection();
			} else {
				logger.info("Failed to lookup datasource.");
			}
		} catch (NamingException ex) {
			logger.info("Cannot get connection: " + ex);
		} catch (SQLException ex) {
			logger.info("Cannot get connection: " + ex);
		}
		return result;
	}

	public String getAll(String cegId) {
		return getAll(cegId, null);
	}

	public String getAll(String cegId, boolean noLeftoutColumn) {
		return getAll(cegId, null, noLeftoutColumn);
	}

	public String getAll(String cegId, String dsJNDI) {
		return getAll(cegId, dsJNDI, false);
	}

	public String getAll(String cegId, String dsJNDI, boolean noLeftoutColumn) {
		Connection connection = null;
		try {
			connection = this.connection(dsJNDI);

			return this.getAll(connection, cegId, noLeftoutColumn);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getFirstByField(String cegId, String fieldName, String fieldType, String fieldValue,
			boolean noLeftoutColumn) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getFirstByField(connection, cegId, fieldName, fieldType, fieldValue, noLeftoutColumn);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getFirstByField(Connection aConnection, String cegId, String fieldName, String fieldType,
			String fieldValue, boolean noLeftoutColumn) throws SQLException {

		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			statement = aConnection.prepareStatement(
					"select * from " + (cegId + "." + this.tableName()) + " where " + fieldName + " = ?");

			switch (fieldType) {
			case "LONG":
				statement.setLong(1, new Long(fieldValue));
				break;
			case "STRING":
				statement.setString(1, fieldValue);
				break;
			case "DATE":
				statement.setDate(1, java.sql.Date.valueOf(fieldValue));
			}

			return executeQueryToJson(statement, resultSet, true, noLeftoutColumn);

		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

	public String getFirstByFields(String cegId, String[] fieldName, String[] fieldType, String[] fieldValue,
			boolean noLeftoutColumn) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getByFields(connection, cegId, fieldName, fieldType, fieldValue, noLeftoutColumn, true);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getByFields(String cegId, String[] fieldName, String[] fieldType, String[] fieldValue,
			boolean noLeftoutColumn) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getByFields(connection, cegId, fieldName, fieldType, fieldValue, noLeftoutColumn, false);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getByFields(Connection aConnection, String cegId, String[] fieldName, String[] fieldType,
			String[] fieldValue, boolean noLeftoutColumn, boolean firstonly) throws SQLException {

		int len = fieldName.length;
		if (len != fieldType.length || len != fieldValue.length) {
			return null;
		}
		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			StringBuilder sb = new StringBuilder("select * from " + (cegId + "." + this.tableName()) + " where ");

			for (int ind = 0; ind < len; ind++) {
				boolean last = ind == len - 1;
				sb.append(fieldName[ind] + " = ? " + (last ? "" : " and "));
			}

			String sql = sb.toString();

			statement = aConnection.prepareStatement(sql);

			for (int ind = 0; ind < len; ind++) {

				switch (fieldType[ind]) {
				case "LONG":
					statement.setLong(ind + 1, new Long(fieldValue[ind]));
					break;
				case "STRING":
					statement.setString(ind + 1, fieldValue[ind]);
					break;
				case "DATE":
					statement.setDate(ind + 1, java.sql.Date.valueOf(fieldValue[ind]));
				}
			}
			return executeQueryToJson(statement, resultSet, firstonly, noLeftoutColumn);

		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

	public String getByField(String cegId, String fieldName, String fieldType, String fieldValue,
			boolean noLeftoutColumn) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getByField(connection, cegId, fieldName, fieldType, fieldValue, noLeftoutColumn);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getBetweenDateAndToday(String cegId, String fieldName, String fieldValue) {
		return getBetweenDateAndToday(cegId, fieldName, fieldValue, null, null);
	}

	public String getBetweenDateAndToday(String cegId, String fieldName, String fieldValue, String fieldName2,
			String fieldValue2) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getBetweenDateAndToday(connection, cegId, fieldName, fieldValue, fieldName2, fieldValue2);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getBetweenDates(String cegId, String fieldName, String dateFrom, String dateTo) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getBetweenDates(connection, cegId, fieldName, dateFrom, dateTo);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getById(String cegId, Identifiable anAggregateRoot, boolean noLeftoutColumn) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.getById(connection, cegId, anAggregateRoot, noLeftoutColumn);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find: " + anAggregateRoot + " because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	protected void delete(String cegId, Identifiable anAggregateRoot) {
		Connection connection = null;
		try {
			connection = this.connection();

			this.delete(connection, cegId, anAggregateRoot);

		} catch (Exception e) {
			throw new RuntimeException("Cannot delete: " + anAggregateRoot + " because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	protected void delete(String cegId, Collection<? extends Identifiable> anAggregateRoots) {

		Connection connection = null;
		try {
			connection = this.connection();

			for (Identifiable root : anAggregateRoots) {
				this.delete(connection, cegId, root);
			}

		} catch (Exception e) {
			throw new RuntimeException("Cannot delete: " + anAggregateRoots + " because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	protected abstract String tableName();

	protected abstract String sequenceName();

	protected abstract Set<String> leftoutColumns();

	public String save(String cegId, Identifiable anAggregateRoot) {
		if (anAggregateRoot.isUnidentified()) {
			return this.insert(cegId, anAggregateRoot);
		} else {
			return this.update(cegId, anAggregateRoot);
		}
	}

	public String upsertByField(String cegId, String fieldName, String fieldType, String fieldValue,
			Identifiable anAggregateRoot) {
		Connection connection = null;
		try {
			connection = this.connection();

			return upsertByField(connection, cegId, fieldName, fieldType, fieldValue, anAggregateRoot);

		} catch (Exception e) {
			throw new RuntimeException("Cannot upsert: " + anAggregateRoot + " because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private String upsertByField(Connection connection, String cegId, String fieldName, String fieldType,
			String fieldValue, Identifiable anAggregateRoot) throws Exception {

		String jsonStr = getFirstByField(connection, cegId, fieldName, fieldType, fieldValue, true);

		// insert
		if (jsonStr == null || "[]".equals(jsonStr)) {
			return insert(connection, cegId, anAggregateRoot);
		}

		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(jsonStr).getAsJsonObject();

		String id = obj.get("id").getAsString();

		anAggregateRoot.setId(Long.valueOf(id));

		update(connection, cegId, anAggregateRoot);

		return id;
	}

	public String upsertByFields(String cegId, String[] fieldName, String[] fieldType, String[] fieldValue,
			Identifiable anAggregateRoot) {
		Connection connection = null;
		try {
			connection = this.connection();

			return upsertByFields(connection, cegId, fieldName, fieldType, fieldValue, anAggregateRoot);

		} catch (Exception e) {
			throw new RuntimeException("Cannot upsert: " + anAggregateRoot + " because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private String upsertByFields(Connection connection, String cegId, String[] fieldName, String[] fieldType,
			String[] fieldValue, Identifiable anAggregateRoot) throws Exception {

		String jsonStr = getByFields(connection, cegId, fieldName, fieldType, fieldValue, true, true);

		// insert
		if (jsonStr == null || "[]".equals(jsonStr)) {
			return insert(connection, cegId, anAggregateRoot);
		}

		JsonParser parser = new JsonParser();
		JsonObject obj = parser.parse(jsonStr).getAsJsonObject();

		String id = obj.get("id").getAsString();

		anAggregateRoot.setId(Long.valueOf(id));

		update(connection, cegId, anAggregateRoot);

		return id;
	}

	private String getById(Connection aConnection, String cegId, Identifiable anAggregateRoot, boolean noLeftoutColumn)
			throws SQLException {

		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			statement = aConnection
					.prepareStatement("select * from " + (cegId + "." + this.tableName()) + " where id = ?");

			statement.setLong(1, anAggregateRoot.identity());
			return executeQueryToJson(statement, resultSet, true, noLeftoutColumn);

		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

	private String getByField(Connection aConnection, String cegId, String fieldName, String fieldType,
			String fieldValue, boolean noLeftoutColumn) throws SQLException {

		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			statement = aConnection.prepareStatement(
					"select * from " + (cegId + "." + this.tableName()) + " where " + fieldName + " = ?");

			switch (fieldType) {
			case "LONG":
				statement.setLong(1, new Long(fieldValue));
				break;
			case "STRING":
				statement.setString(1, fieldValue);
			}

			return executeQueryToJson(statement, resultSet, false, noLeftoutColumn);

		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

	private String getBetweenDateAndToday(Connection aConnection, String cegId, String fieldName, String fieldValue,
			String fieldName2, String fieldValue2) throws SQLException {

		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			statement = aConnection
					.prepareStatement("select * from " + (cegId + "." + this.tableName()) + " where " + fieldName
							+ " between ? and now() " + ((fieldName2 != null) ? " AND  " + fieldName2 + " = ? " : ""));

			String dateFrom = dateWithDash(fieldValue);
			statement.setDate(1, java.sql.Date.valueOf(dateFrom));
			if (fieldName2 != null) {
				statement.setString(2, fieldValue2);
			}

			return executeQueryToJson(statement, resultSet, false, true);

		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

	private static String dateWithDash(String dateStr) {
		if (dateStr.length() == 8) {
			String ret = dateStr.substring(0, 4) + "-" + dateStr.substring(4, 6) + "-" + dateStr.substring(6, 8);
			return ret;
		}
		return dateStr;
	}

	private String getBetweenDates(Connection aConnection, String cegId, String fieldName, String dateFrom,
			String dateTo) throws SQLException {

		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			statement = aConnection.prepareStatement(
					"select * from " + (cegId + "." + this.tableName()) + " where " + fieldName + " between ? and ? ");

			statement.setDate(1, java.sql.Date.valueOf(dateWithDash(dateFrom)));
			statement.setDate(2, java.sql.Date.valueOf(dateWithDash(dateTo)));

			return executeQueryToJson(statement, resultSet, false, true);

		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

	private String getAll(Connection aConnection, String cegId, boolean noLeftoutColumn) throws SQLException {

		PreparedStatement statement = null;

		ResultSet resultSet = null;
		try {
			statement = aConnection.prepareStatement("select * from " + (cegId + "." + this.tableName()));

			return executeQueryToJson(statement, resultSet, false, noLeftoutColumn);

		} finally {
			this.close(resultSet);
			this.close(statement);
		}
	}

	protected String executeQueryToJson(PreparedStatement statement, ResultSet resultSet, boolean first,
			boolean noLeftoutColumn) throws SQLException {
		resultSet = statement.executeQuery();

		List<Map<String, Object>> list = getEntitiesFromResultSet(resultSet, noLeftoutColumn);

		// All the date and time is string
		Gson gson = new GsonBuilder().create();

		String json = gson.toJson((first && list.size() > 0) ? list.get(0) : list);

		String regex = "\\";
		String replacement = "";
		String json1 = json.replaceAll("\\\\", "").replaceAll("\"\\{", "\\{").replaceAll("\\}\"", "\\}");
		return json1;
	}

	private void delete(Connection aConnection, String cegId, Identifiable anAggregateRoot) throws SQLException {

		PreparedStatement statement = null;

		try {
			statement = aConnection
					.prepareStatement("delete from " + (cegId + "." + this.tableName()) + " where id = ?");

			statement.setLong(1, anAggregateRoot.identity());
			statement.executeUpdate();

		} finally {
			this.close(statement);
		}
	}

	private String insert(String cegId, Identifiable anAggregateRoot) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.insert(connection, cegId, anAggregateRoot);

		} catch (Exception e) {
			throw new RuntimeException("Cannot save: " + anAggregateRoot + " because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private String insert(Connection aConnection, String cegId, Identifiable anAggregateRoot) throws Exception {

		PreparedStatement statement = null;

		ResultSet rs = null;

		try {
			JsonObject json = anAggregateRoot.getData();

			Map<String, String> columns = getColumns(cegId);

			String sql = createInsertSql(cegId, columns, json);

			statement = aConnection.prepareStatement(sql);

			rs = statement.executeQuery();

			rs.next();

			Long id = rs.getLong(1);

			return ("" + id);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			this.close(statement);
		}
	}

	private String update(String cegId, Identifiable anAggregateRoot) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.update(connection, cegId, anAggregateRoot);

		} catch (Exception e) {
			throw new RuntimeException("Cannot update: " + anAggregateRoot + " because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private String update(Connection aConnection, String cegId, Identifiable anAggregateRoot) throws SQLException {

		PreparedStatement statement = null;

		try {
			JsonObject json = anAggregateRoot.getData();

			Map<String, String> columns = getColumns(cegId);

			String sql = createUpdateSql(cegId, columns, json);

			logger.info("------------------ " + sql);

			statement = aConnection.prepareStatement(sql);

			statement.setLong(1, anAggregateRoot.identity());

			statement.executeUpdate();

			return ("" + anAggregateRoot.identity());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e.getLocalizedMessage());
		} finally {
			this.close(statement);
		}

	}

	private Map<String, String> getColumns(String cegId) {

		if (cacheColumns && tableColumns.containsKey(tableName())) {
			Map<String, String> ret = tableColumns.get(tableName());
			if (ret.size() > 0) {
				return tableColumns.get(tableName());
			}
		}

		Map<String, String> columns = new HashMap<>();
		Connection connection = null;
		ResultSet resultSet = null;
		PreparedStatement statement = null;
		try {
			connection = this.connection();

			String sql = "SELECT column_name, data_type " + "FROM information_schema.columns "
					+ "WHERE table_schema = '" + cegId + "'" + "  AND table_name   = '" + tableName().toLowerCase()
					+ "'";
			statement = connection.prepareStatement(sql);

			resultSet = statement.executeQuery();

			while (resultSet.next()) {
				String name = resultSet.getString("COLUMN_NAME");
				String type = resultSet.getString("DATA_TYPE");

				// System.out.println("Column name: [" + name + "]; type: [" + type + "]");
				columns.put(name, type);
			}
			tableColumns.put(tableName(), columns);
			return columns;
		} catch (Exception e) {
			return null;
		} finally {
			assert resultSet != null;
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			assert connection != null;
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	protected String createInsertSql(String cegId, Map<String, String> columns, JsonObject json) throws ParseException {
		StringBuilder sb = new StringBuilder();
		StringBuilder values = new StringBuilder();

		values.append(" VALUES (nextval('" + cegId + "." + sequenceName() + "'), ");
		sb.append("INSERT INTO " + cegId + "." + tableName() + " (id, ");

		Set<Map.Entry<String, JsonElement>> entrySet = json.entrySet();
		int size = entrySet.size();
		boolean first = true;
		for (Map.Entry<String, JsonElement> entry : entrySet) {
			String key = entry.getKey();
			String value = getValueAsString(json, key);// json.get(key).getAsString();
			if ("id".equals(key) || !columns.containsKey(key.toLowerCase())) {
				continue;
			}
			// logger.info("key= " + key + " = " + value);
			sb.append((first ? " " : " , ") + key);
			String type = columns.get(key.toLowerCase());
			if (type.indexOf("timestamp") > -1) {
				value = convertBaseTimestampStrToPostgreFormat(value);
			}
			boolean textlike = type.indexOf("timestamp") > -1 || type.indexOf("json") > -1 || type.indexOf("char") > -1
					|| type.indexOf("text") > -1 || type.indexOf("date") > -1 || type.indexOf("USER") > -1;

			String surrand = (textlike ? "'" : "");
			values.append((first ? " " : " , ") + surrand + value + surrand);
			first = false;
		}

		sb.append(") ");
		String sql = sb.toString() + values.toString() + ") RETURNING id ";
		// logger.info(sql);
		return sql;
	}

	final String TimeFormatOrig = "yyyyMMdd HHmmss";
	final String TimeFormat = "yyyy-MM-dd HH:mm:ss.SSSX";

	private String convertBaseTimestampStrToPostgreFormat(String value) throws ParseException {
//		SimpleDateFormat sdfo = new SimpleDateFormat(TimeFormatOrig);
//		SimpleDateFormat sdf = new SimpleDateFormat(TimeFormat);

		DateTimeFormatter formatterOrig = DateTimeFormatter.ofPattern(TimeFormatOrig);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TimeFormat);

//		Date d;
//		try {
//			d = sdfo.parse(value);
//		} catch (Exception e) {
//			return value;
//		}
//		String ret = sdf.format(d);
		LocalDateTime d;
		try {
			d = LocalDateTime.parse(value, formatterOrig);
		} catch (Exception e) {
			return value;
		}
		String ret = d.format(formatter);
		return ret;
	}

	protected String createUpdateSql(String cegId, Map<String, String> columns, JsonObject json) {
		StringBuilder sb = new StringBuilder();

		sb.append("UPDATE " + cegId + "." + tableName() + " ");

		Set<Map.Entry<String, JsonElement>> entrySet = json.entrySet();
		int size = entrySet.size();
		int ind = 0;
		boolean first = true;
		for (Map.Entry<String, JsonElement> entry : entrySet) {
			ind++;
			String key = entry.getKey();
			String value = getValueAsString(json, key);// json.get(key).getAsString();
			if ("id".equals(key) || (!columns.containsKey(key) && !columns.containsKey(key.toLowerCase()))) {
				continue;
			}
			String type = columns.get(key.toLowerCase());
			boolean textlike = type.indexOf("json") > -1 || type.indexOf("ARRAY") > -1 || type.indexOf("char") > -1
					|| type.indexOf("text") > -1 || type.indexOf("timestamp") > -1 || type.indexOf("date") > -1
					|| type.indexOf("USER") > -1;
			String surrand = (textlike ? "'" : "");

			sb.append((first ? " SET " : " , ") + key + " = " + surrand + value + surrand);
			first = false;
		}

		sb.append(" where id = ?");
		String sql = sb.toString();
		logger.info(sql);
		return sql;
	}

	private String getValueAsString(JsonObject json, String key) {
		String value = "";
		try {
			value = json.get(key).getAsString();
		} catch (Exception e) {
			JsonElement jk = json.get(key);
			if (jk.isJsonObject()) {
				value = jk.getAsJsonObject().toString();
				logger.info("----------------- " + value);
			}
			if (jk.isJsonArray()) {
				value = jk.getAsJsonArray().toString();
				logger.info("----------------- " + value);
			}
		}
		if (value.contains("'")) {
			return value.replaceAll("'", "''");
		}
		return value;
	}

	protected List<Map<String, Object>> getEntitiesFromResultSet(ResultSet resultSet, boolean noLeftoutColumn)
			throws SQLException {
		ArrayList<Map<String, Object>> entities = new ArrayList<>();
		while (resultSet.next()) {
			entities.add(getEntityFromResultSet(resultSet, noLeftoutColumn));
		}
		return entities;
	}

	protected Map<String, Object> getEntityFromResultSet(ResultSet resultSet, boolean noLeftoutColumn)
			throws SQLException {
		ResultSetMetaData metaData = resultSet.getMetaData();
		int columnCount = metaData.getColumnCount();
		Map<String, Object> resultsMap = new HashMap<>();
		for (int i = 1; i <= columnCount; ++i) {
			String columnName = metaData.getColumnName(i).toLowerCase();
			if (!noLeftoutColumn && leftoutColumns().contains(columnName)) {
				continue;
			}
			Object object = resultSet.getObject(i);
			String objStr = resultSet.getString(i);

			if (object != null && (object.getClass().getCanonicalName().indexOf("PGobject") > -1
					|| object.getClass().getCanonicalName().indexOf("Timestamp") > -1
					|| object.getClass().getCanonicalName().indexOf("Date") > -1)) {

				object = objStr;
			}
			resultsMap.put(columnName, object);
		}
		return resultsMap;
	}

}
