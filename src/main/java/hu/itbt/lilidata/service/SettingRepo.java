package hu.itbt.lilidata.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

public class SettingRepo extends AbstractRepo {

	@Override
	protected String tableName() {
		return "SETTINGS";
	}

	@Override
	protected String sequenceName() {
		return "settings_id_seq";
	}

	@Override
	protected Set<String> leftoutColumns() {
		Set<String> cols = new HashSet<>();
		return cols;
	}

	public boolean deleteEvent(String cegId, long eventId) {
		Connection connection = null;
		try {
			connection = this.connection();

			return this.deleteEvent(connection, cegId, eventId);

		} catch (Exception e) {
			throw new RuntimeException("Cannot find any, because: " + e.getMessage());
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public boolean deleteEvent(Connection aConnection, String cegId, long eventId) {
		PreparedStatement statement = null;
		try {
			statement = aConnection
					.prepareStatement("delete from " + (cegId + "." + this.tableName()) + " where id = ?");

			statement.setLong(1, eventId);

			statement.executeUpdate();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} finally {
			this.close(statement);
		}

	}

}
