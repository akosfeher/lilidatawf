package hu.itbt.lilidata.utils;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;

import hu.itbt.lilidata.model.AuthClaims;
import hu.itbt.lilidata.model.Jsonable;
import hu.itbt.lilidata.service.LogEvent;
import hu.itbt.lilidata.service.LogEventTypesInterface;

@Interceptor
@EventLog
public class EventLogInterceptor implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(EventLogInterceptor.class);

	@Inject
	LogEvent logEvent;

//	@Context
//	  ResourceInfo resourceInfo;

	public EventLogInterceptor() {
		super();
	}

	@AroundInvoke
	public Object roleMethodeEntry(InvocationContext ctx) throws Exception {
		EventLog el = ctx.getMethod().getAnnotation(EventLog.class);
		Object[] paramValues = ctx.getParameters();

		// Class<?>[] paramTypes = ctx.getMethod().getParameterTypes();
		LogEventTypesInterface eventType = LogEventTypesInterface.valueOf(/* "USER_BYID" */el.value());
		String target = ctx.getTarget().toString().substring(0, ctx.getTarget().toString().indexOf("$Proxy$"));

		long beforeTime = System.currentTimeMillis();
		Object obj = null;
		String message = "OK";
		int status = 200;
		try {
			obj = ctx.proceed();
			if (obj instanceof Response) {
				status = ((Response) obj).getStatus();
				Object entity = ((Response) obj).getEntity();
				if (entity instanceof String) {
					message = (String) entity;
				}
				logger.info(entity);
			}
			return obj;
		} finally {
			int payloadInd = paramValues.length - 1;
			String cegId = (String) paramValues[0];
			String token = (String) paramValues[1];
			;
			String payload = serializePayload(paramValues[payloadInd]);

			String role = "UNKNOWN";
			String id = "0";

			try {
				AuthClaims auth = Utils.getAuth(token);
				role = auth.getRole();
				id = auth.getId();
				String cegIdToken = auth.getCeg();
			} catch (Exception e) {
				logger.warn("#### invalid token : " + token);
			}

			logger.info("token= " + token);
			long time = System.currentTimeMillis() - beforeTime;
			// System.out.println("A " + target + " osztály "+ctx.getMethod().getName() + "
			// metódus végrehajtási ideje "+time+" msec");

			logEvent.addEvent(status, message, cegId, eventType, Long.valueOf(id), role, payload);

			logger.info(eventType + " logtype, a " + target + " class " + ctx.getMethod().getName()
					+ " methode execution time " + time + " msec");
		}
	}

	private String serializePayload(Object payload) {
		if (payload instanceof Jsonable) {
			return ((Jsonable) payload).getJsonString();
		}
		if (payload == null) {
			return "null";
		} else {
			return payload.toString().replaceAll("'", "\"");
		}
	}

}