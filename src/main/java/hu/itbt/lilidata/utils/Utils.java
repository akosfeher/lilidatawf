package hu.itbt.lilidata.utils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;
import java.util.Date;
import java.util.Map;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.xml.bind.DatatypeConverter;

import org.jboss.logging.Logger;

import com.google.gson.Gson;

import hu.itbt.lilidata.model.AuthClaims;
import hu.itbt.lilidata.service.UserRepo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

public class Utils {

	protected final static String jsonWebTokenKey = "vHVP/engBteZ2ivop3kZhrOHD/NFtez2EOI/Ad9aMYM=";

	private static final Logger logger = Logger.getLogger(Utils.class);

	protected static UserRepo userRepo;

	protected static UserRepo getUserRepo() {
		if (userRepo == null) {
			userRepo = new UserRepo();
		}
		return userRepo;
	}

	public static String createHash(String pw, String salt) {
		byte[] saltArr = DatatypeConverter.parseHexBinary(salt);

		PBEKeySpec spec = new PBEKeySpec(pw.toCharArray(), saltArr, 1000, 2048);
		SecretKeyFactory skf = null;
		byte[] hashArr = null;
		try {
			skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			hashArr = skf.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}

		return DatatypeConverter.printHexBinary(hashArr);
	}

	public static String getRandomSalt() {
		SecureRandom sr = null;
		try {
			sr = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		String saltHex = DatatypeConverter.printHexBinary(salt);
		return saltHex;
	}

	public static String createJwt(AuthClaims auth, Long expMsec) {
		byte[] decodedKey = Base64.getDecoder().decode(jsonWebTokenKey);
		// rebuild key using SecretKeySpec
		SecretKey skeySpecPriv = new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		JwtBuilder builder = Jwts.builder().setIssuedAt(now).setSubject(auth.getSubject()).setId(auth.getId())
				.claim("role", auth.getRole()).claim("ceg", auth.getCeg()).claim("email", auth.getEmail())
				.claim("username", auth.getUsername()).claim("name", auth.getName())
				.signWith(SignatureAlgorithm.HS256, skeySpecPriv);

		// if it has been specified, let's add the expiration
		if (expMsec != null && expMsec >= 0) {
			long expMillis = nowMillis + expMsec;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		String jwtString = builder.compact();

		System.out.println("jwtString = " + jwtString);

		return jwtString;
	}

	public static AuthClaims getJwtClaims(String jwtString) {
		byte[] decodedKey = Base64.getDecoder().decode(jsonWebTokenKey);
		// rebuild key using SecretKeySpec
		SecretKey skeySpecPriv = new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");

		Claims claims = Jwts.parser().setSigningKey(skeySpecPriv).parseClaimsJws(jwtString).getBody();
		String id = claims.getId();
		String subject = claims.getSubject();
		String email = claims.get("email").toString();
		String role = claims.get("role").toString();
		String name = claims.get("name").toString();
		// String username = claims.get("username").toString();
		String ceg = claims.get("ceg").toString();

		AuthClaims auth = new AuthClaims(id, ceg, role, subject, email, name, "");

		return auth;
	}

	public static boolean isJwtValid(String jwtString) {
		byte[] decodedKey = Base64.getDecoder().decode(jsonWebTokenKey);
		// rebuild key using SecretKeySpec
		SecretKey skeySpecPriv = new SecretKeySpec(decodedKey, 0, decodedKey.length, "HmacSHA256");

		Date exp = null;
		try {
			Claims claims = Jwts.parser().setSigningKey(skeySpecPriv).parseClaimsJws(jwtString).getBody();
			exp = claims.getExpiration();
		} catch (ExpiredJwtException e) {
			return false;
		} catch (UnsupportedJwtException e) {
			e.printStackTrace();
			return false;
		} catch (MalformedJwtException e) {
			e.printStackTrace();
			return false;
		} catch (SignatureException e) {
			e.printStackTrace();
			return false;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return false;
		}

		System.out.println("expiration = " + exp);

		return true;
	}

	public static AuthClaims getAuth(String token) {
		Base64.Decoder decoder = Base64.getUrlDecoder();
		String[] parts = token.split("\\."); // Splitting header, payload and signature
		String header = new String(decoder.decode(parts[0]));
		String payload = new String(decoder.decode(parts[1]));

		System.out.println("Headers: " + header); // Header
		System.out.println("Payload: " + payload); // Payload

		Gson gson = new Gson();
		AuthClaims auth = gson.fromJson(payload, AuthClaims.class);

		return auth;
	}

	/**
	 * get AuthClaims from token and header
	 * 
	 * @param tokenForAuth
	 * @param headers
	 * @return
	 */
	public static AuthClaims checkAuth(String tokenForAuth, HttpHeaders headers) {
		return checkAuth(tokenForAuth, headers, false);
	}

	/**
	 * get AuthClaims from token and header
	 * 
	 * @param tokenForAuth
	 * @param headers
	 * @param expired
	 * @return
	 */
	public static AuthClaims checkAuth(String tokenForAuth, HttpHeaders headers, boolean expired) {
		Map<String, Cookie> cookies = headers.getCookies();
		if (tokenForAuth == null || tokenForAuth.isEmpty()) {
			if (cookies != null && cookies.get("x-access-token") != null) {
				tokenForAuth = cookies.get("x-access-token").getValue();
			}
		}
		boolean valid = Utils.isJwtValid(tokenForAuth);

		if (!valid && !expired) {
			return null;
		}
		AuthClaims auth = null;
		try {
			auth = Utils.getJwtClaims(tokenForAuth);
		} catch (Exception e) {
			logger.error("{\"valid\":" + valid + ",   \"error\": \"" + e.getMessage() + "\"}");
			return null;
			// return Response.status(500).entity("{\"valid\":"+valid+", \"error\":
			// \""+e.getMessage() + "\"}").build();
		}
		return auth;
	}

	public static String dateWithDash(String dateStr) {
		if (dateStr.length() == 8) {
			String ret = dateStr.substring(0, 4) + "-" + dateStr.substring(4, 6) + "-" + dateStr.substring(6, 8);
			return ret;
		}
		return dateStr;
	}

}
