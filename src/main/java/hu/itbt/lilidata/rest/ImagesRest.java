package hu.itbt.lilidata.rest;

import javax.ws.rs.Path;

@Path("/workdays")
public class ImagesRest {

//	@Inject
//	protected WorkdaysRepo workdaysRepo;
//
//	protected WorkdaysRepo getWorkdaysRepo() {
//		if (workdaysRepo == null) {
//			workdaysRepo = new WorkdaysRepo();
//		}
//		return workdaysRepo;
//	}

//	// date format yyyy-mm-dd
//	@GET
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/{cegId}/{date}")
//	@EventLog(value = "WORKDAY")
//	public Response getWorkdayStatusAtDate(@PathParam("cegId") String cegId, @PathParam("date") String dateStr) {
//
//		String ret = getWorkdaysRepo().getFirstByField(cegId, "DATE", "DATE", dateStr, false);
//
//		String wd = "WORKDAY";
//		if (ret == null || "[]".equals(ret)) {
//			LocalDate localDate = LocalDate.parse(dateStr);
//			wd = (localDate.getDayOfWeek().getValue() < 6) ? "WORKDAY" : "HOLIDAY";
//		} else {
//			Gson gson = new GsonBuilder().setDateFormat("MMM d, yyyy").create();
//
//			Workday workdayObj = gson.fromJson(ret, Workday.class);
//			wd = workdayObj.isWorkday() ? "WOKDAY" : "HOLIDAY";
//		}
//		return Response.status(200).entity(wd).build();
//	}

//	@GET
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("/{cegId}/since/{date}")
//	@EventLog(value = "WORKDAYS_SINCE")
//	public Response getNumOfWorkdays(@PathParam("cegId") String cegId, @PathParam("date") String dateStr) {
//
//		String jsonArray = getWorkdaysRepo().getBetweenDateAndToday(cegId, "DATE", dateStr);
//
//		// database workday entries
//		List<Workday> workdays = new ArrayList<>();
//		if (jsonArray != null) {
//			Type listType = new TypeToken<ArrayList<Workday>>() {
//			}.getType();
//			Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new GsonLocalDateAdapter()).create();
//			workdays = gson.fromJson(jsonArray, listType);
//		}
//
//		Map<LocalDate, Boolean> dbWorkdaysMap = new HashMap<>();
//		if (workdays.size() > 0) {
//			for (Workday wd : workdays) {
//				dbWorkdaysMap.put(wd.getLocalDate(), wd.isWorkday());
//			}
//		}
//		LocalDate startDate = LocalDate.parse(dateStr);
//		LocalDate endDate = LocalDate.now().plusDays(1);
//		int numOfWorkdays = 0;
//		for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
//			boolean isWorkday = date.getDayOfWeek().getValue() < 6;
//			if (dbWorkdaysMap.containsKey(date)) {
//				isWorkday = dbWorkdaysMap.get(date);
//			}
//			if (isWorkday) {
//				numOfWorkdays++;
//			}
//		}
//
//		return Response.status(200).entity(numOfWorkdays).build();
//	}

}