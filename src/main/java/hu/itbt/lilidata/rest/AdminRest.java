package hu.itbt.lilidata.rest;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

import hu.itbt.lilidata.ApplicationConfig;
import hu.itbt.lilidata.model.AuthClaims;
import hu.itbt.lilidata.model.User;
import hu.itbt.lilidata.service.Identifiable;
import hu.itbt.lilidata.service.SendEmail;
import hu.itbt.lilidata.service.UserRepo;
import hu.itbt.lilidata.utils.EventLog;
import hu.itbt.lilidata.utils.GsonUTCDateAdapter;
import hu.itbt.lilidata.utils.JWTRoleNeeded;
import hu.itbt.lilidata.utils.Utils;

@Path("/admin")
@ApplicationScoped
//@JWTRoleNeeded({"LOVACSKA", "CICA"})
public class AdminRest {

	private static Logger logger = Logger.getLogger(AdminRest.class.getName());

	public static final boolean NO_LEFTOUT_COLUMN = true;

	@PostConstruct
	public void init() {
		System.out.println("UserRest created");
	}

	public AdminRest() {
		super();
	}

	// @Inject
	protected static UserRepo userRepo;

	protected static UserRepo getUserRepo() {
		if (userRepo == null) {
			userRepo = new UserRepo();
		}
		return userRepo;
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces({ "image/png", "image/jsp", "image/gif" })
	@Path("/{cegId}/{userId}/portrait")
	// @JWTRoleNeeded({"ADMIN", "USER", "CEGADMIN"})
	// @EventLog(value="USER_BYID")
	public Response getUserPortrait(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@PathParam("userId") long userId, @QueryParam("size") int size) throws IOException {

		Identifiable ifa = new Identifiable(userId);

		byte[] portrait = getUserRepo().getPortrait(cegId, userId);

		if (size > 5) {
			byte[] smallportrait = resizeImageAsJPG(portrait, size);

			ResponseBuilder response = Response.ok(smallportrait);
			return response.build();
		} else {
			ResponseBuilder response = Response.ok(portrait);
			return response.build();

		}
		// response.header("Content-Disposition", "attachment; filename=portrait.gif");

	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	// @Produces({ "image/png", "image/jsp", "image/gif" })
	@Path("/email/{to}/{subject}/{content}")
	// @JWTRoleNeeded({"ADMIN", "USER", "CEGADMIN"})
	// @EventLog(value="USER_BYID")
	public Response sendEmail(@PathParam("to") String to, @PathParam("subject") String subject,
			@PathParam("content") String content, @HeaderParam("x-access-token") String token) {

		SendEmail.sendSimpleEmail(true, to, subject, "<html><body><h3>" + content + "</h3></body></html>", content,
				false);

		ResponseBuilder response = Response.ok("ok");
		// response.header("Content-Disposition", "attachment; filename=portrait.gif");
		return response.build();
	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	// @Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/{userId}/portrait")
	// @JWTRoleNeeded({"ADMIN", "USER", "CEGADMIN"})
	// @EventLog(value="USER_BYID")
	public Response putUserUserPortrait(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@PathParam("userId") long userId, MultipartFormDataInput input) {

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("portrait");

		InputPart inputPart = inputParts.get(0);

		try {

			// convert the uploaded file to inputstream
			InputStream inputStream = inputPart.getBody(InputStream.class, null);

			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[1024];
			while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, nRead);
			}

			buffer.flush();
			byte[] bytes = buffer.toByteArray();

			String ret = getUserRepo().updatePortrait(cegId, userId, bytes);

			System.out.println("Done");

			return Response.status(200).entity(ret).build();

		} catch (IOException e) {
			e.printStackTrace();
			return Response.status(500).entity(e.getLocalizedMessage()).build();
		}

	}

	/**
	 * This method takes in an image as a byte array (currently supports GIF, JPG,
	 * PNG and possibly other formats) and resizes it to have a width no greater
	 * than the pMaxWidth parameter in pixels. It converts the image to a standard
	 * quality JPG and returns the byte array of that JPG image.
	 * 
	 * @param pImageData the image data.
	 * @param pMaxWidth  the max width in pixels, 0 means do not scale.
	 * @return the resized JPG image.
	 * @throws IOException if the iamge could not be manipulated correctly.
	 */
	public byte[] resizeImageAsJPG(byte[] pImageData, int pMaxWidth) throws IOException {
		// Create an ImageIcon from the image data
		ImageIcon imageIcon = new ImageIcon(pImageData);
		int width = imageIcon.getIconWidth();
		int height = imageIcon.getIconHeight();
		logger.info("imageIcon width: " + width + "  height: " + height);
		// If the image is larger than the max width, we need to resize it
		if (pMaxWidth > 0 && width > pMaxWidth) {
			// Determine the shrink ratio
			double ratio = (double) pMaxWidth / imageIcon.getIconWidth();
			logger.info("resize ratio: " + ratio);
			height = (int) (imageIcon.getIconHeight() * ratio);
			width = pMaxWidth;
			logger.info("imageIcon post scale width: " + width + "  height: " + height);
		}
		// Create a new empty image buffer to "draw" the resized image into
		BufferedImage bufferedResizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		// Create a Graphics object to do the "drawing"
		Graphics2D g2d = bufferedResizedImage.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
		// Draw the resized image
		g2d.drawImage(imageIcon.getImage(), 0, 0, width, height, null);
		g2d.dispose();
		// Now our buffered image is ready
		// Encode it as a JPEG

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bufferedResizedImage, "jpg", baos);
		byte[] resizedImageByteArray = baos.toByteArray();

//		ByteArrayOutputStream encoderOutputStream = new ByteArrayOutputStream();
//		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(encoderOutputStream);
//		encoder.encode(bufferedResizedImage);
//		byte[] resizedImageByteArray = encoderOutputStream.toByteArray();
		return resizedImageByteArray;
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/{userId}")
	@JWTRoleNeeded({ "ADMIN", "USER", "CEGADMIN" })
	@EventLog(value = "USER_BYID")
	public Response searchUserById(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@PathParam("userId") long userId, String payload) {

		Identifiable ifa = new Identifiable(userId);

		String ret = getUserRepo().getById(cegId, ifa, false);
		return Response.status(200).entity(ret).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/free/players")
	// @JWTRoleNeeded({ "ADMIN", "USER", "CEGADMIN" })
	// @EventLog(value = "USER_BYID")
	public Response searchFreeUser(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			String payload) {

		Gson gson = new Gson();

		String ret = getUserRepo().getFreePlayers(cegId);

		Type listType = new TypeToken<ArrayList<User>>() {
		}.getType();
		List<User> users = gson.fromJson(ret, listType);

		StringBuilder sb = new StringBuilder();
		for (User u : users) {
			sb.append(u.getName() + "," + u.getEmail() + "\n");
		}
		return Response.status(200).entity(sb.toString()).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response getAllUsers(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@QueryParam("mode") String mode, String payload) {

		String ret = getUserRepo().getAll(cegId, true);

		Gson gson = new Gson();
		JsonParser parser = new JsonParser();

		List<User> users = gson.fromJson(ret, new TypeToken<List<User>>() {
		}.getType());

		for (User user : users) {
			if ("notactive".equals(mode) && user.getActive_game_id() > 0) {
				continue;
			}
			user.setRegistered(user.getHash() != null && !user.getHash().isEmpty());
			user.setHash("");
			user.setSalt("");
			if ("UNKNOWN".equals(user.getRole())) {
				user.setRole("");
			}
		}
		String ret1 = gson.toJson(users);

		return Response.status(200).entity(ret1).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/users")
	// @JWTRoleNeeded({"ADMIN", "CEGADMIN"})
	// @EventLog(value = "USER_ALL")
	public Response getCompanyUsrs(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String token,
			@QueryParam("mode") String mode, String payload) {

		String ret = getUserRepo().getAll(cegId, true);

		Gson gson = new Gson();
		JsonParser parser = new JsonParser();

		List<User> users = gson.fromJson(ret, new TypeToken<List<User>>() {
		}.getType());

		List<User> notactives = new ArrayList<>();

		for (User user : users) {
			if ("notactive".equals(mode) && user.getActive_game_id() > 0) {
				continue;
			}
			user.setRegistered(user.getHash() != null && !user.getHash().isEmpty());
			user.setHash("");
			user.setSalt("");
			if ("UNKNOWN".equals(user.getRole())) {
				user.setRole("");
			}
			notactives.add(user);
		}
		String ret1 = gson.toJson(notactives);

		return Response.status(200).entity(ret1).build();
	}

	@POST
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/newpw")
	// @JWTRoleNeeded({"NONE"})
	// @EventLog(value="USER_NEWPW")
	public Response newpwUser(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String tokenForAuth,
			User payload) {

		User user = payload;

		String usr = getUserRepo().getFirstByField(cegId, "email", "STRING", user.getEmail(), NO_LEFTOUT_COLUMN);

		if (usr.length() < 10) {
			return Response.status(401).entity("User does not exist.").build();
		}

		Gson gson = new GsonBuilder().create();
		User usrObj = gson.fromJson(usr, User.class);

		String id = "" + usrObj.getId();

		String email = user.getEmail();
		String name = usrObj.getName();
		String role = usrObj.getRole();
		String mode = "newpw";

		String token = sendNepwRegEmail(cegId, mode, email, name, role, id);

		return Response.status(200)
				.entity("{\"status\": \"OK\",   \"id\": \"" + id + "\", \"token\": \"" + token + "\"}").build();

	}

	private String sendNepwRegEmail(String cegId, String mode, String email, String name, String role, String id) {
		AuthClaims auth = new AuthClaims(id, cegId, role, mode, email, name, "");

		boolean newpw = "newpw".equals(mode);

		String token = Utils.createJwt(auth, (long) (30 * 60 * 1000));

		String html = "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "<head>\r\n"
				+ "    <meta charset=\"UTF-8\">\r\n" + "    <title>Title</title>\r\n" + "</head>\r\n" + "<body>";

		String content = "<h3>Game4Team " + (newpw ? "change password" : "register") + "</h3>\r\n"
				+ "Please click on this link <a href=";
		String contentEnd = ">" + (newpw ? "password change" : "registration") + "</a> to set your password.\r\n"
				+ "<br /><br />\r\n" + "Thank you.</body></html>";

		if (email.indexOf("test") > -1) {
			String testEmail = ApplicationConfig.getInstance().getProperty("TEST_EMAIL");
			// String testEmail = System.getProperty("TEST_EMAIL", "feher@itbt.hu");
			email = testEmail;
		}

		String clientURL = ApplicationConfig.getInstance().getProperty("CLIENT_URL");

		if (clientURL == null || clientURL.isEmpty()) {
			clientURL = "http://localhost:3030";
		}

		logger.info("sendNepwRegEmail  -  clientURL (CLIENT_URL): " + clientURL);

		String textMessage = "Your browser does not support html mail. Please click on the following link to "
				+ (newpw ? "change your password" : "register: ") + " " + clientURL + "/portal/newpw?conf=" + token;
		boolean htmlemail = true;
		SendEmail.sendSimpleEmail(htmlemail, email, "G4T " + (newpw ? "reset password" : "registration"),
				html + content + clientURL + "/portal/newpw?conf=" + token + contentEnd, textMessage, false);
		return token;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/login")
	// @JWTRoleNeeded({"NONE"})
	@EventLog(value = "USER_LOGIN")
	public Response loginUser(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String tokenForAuth,
			User payload) {

		User user = payload;

		String usr = getUserRepo().getFirstByField(cegId, "email", "STRING", user.getEmail(), true);

		if (usr.length() < 10) {
			return Response.status(401).entity("Wrong credentials, no email found.").build();
		}

		Gson gson = new Gson();
		User usrObj = gson.fromJson(usr, User.class);

		if (!usrObj.isActive()) {
			return Response.status(401).entity("The user is not active.").build();
		}

//        user.setSalt(usrObj.getSalt());
//        user.setHash(usrObj.getHash());

		if (!usrObj.checkPw(user.getPassword())) {
			return Response.status(401).entity("Wrong credentials.").build();
		}

		AuthClaims auth = new AuthClaims(("" + usrObj.getId()), cegId, usrObj.getRole(), "login", usrObj.getEmail(),
				usrObj.getName(), usrObj.getUsername());

		String token = Utils.createJwt(auth, (long) (300 * 60 * 1000));

		usrObj.setSalt("");
		usrObj.setHash("");
		String u = gson.toJson(usrObj); // usr;// .replaceAll("\"","\\\\\"");
		// System.out.println(u);
		return Response.status(200).entity("{\"token\": \"" + token + "\",   \"user\": " + u + "}").build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/auth")
	// @JWTRoleNeeded({"NONE"})
	@EventLog(value = "USER_LOGIN")
	public Response authUser(@PathParam("cegId") String cegId, @HeaderParam("x-access-token") String tokenForAuth,
			@QueryParam("p") String p, String payload) {

		byte[] decodedUsername = Base64.getDecoder().decode(p);

		String username = new String(decodedUsername);

		String usr = getUserRepo().getFirstByField(cegId, "email", "STRING", username, false);

		if (usr.length() < 10) {
			return Response.status(401).entity("No user find with this email address.").build();
		}

		Gson gson = new Gson();
		User usrObj = gson.fromJson(usr, User.class);

		if (!usrObj.isActive()) {
			return Response.status(401).entity("User is not active.").build();
		}

		AuthClaims auth = new AuthClaims(("" + usrObj.getId()), cegId, usrObj.getRole(), "login", usrObj.getEmail(),
				usrObj.getName(), usrObj.getUsername());

		String token = Utils.createJwt(auth, (long) (300 * 60 * 1000));

		String u = usr;// .replaceAll("\"","\\\\\"");
		// System.out.println(u);
		return Response.status(200).entity("{\"token\": \"" + token + "\",   \"user\": " + u + "}").build();
	}

	/**
	 * Adds the company users.
	 *
	 * @param cegId        the ceg id
	 * @param tokenForAuth the token for auth
	 * @param csvStr       the csv str
	 * @return the response
	 */
	@POST
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/companies/{cegId}/users")
	// @EventLog(value = "COMPANY_BASEDATA")
	// @JWTRoleNeeded({"ADMIN"})
	public Response addCompanyUsers(@PathParam("cegId") String cegId,
			@HeaderParam("x-access-token") String tokenForAuth, String csvStr) {

		logger.info("users upload:\n" + csvStr);

		JsonParser parser = new JsonParser();
		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

		Scanner scanner = new Scanner(csvStr);
		scanner.useDelimiter(";");

		StringBuilder sb = new StringBuilder();

		while (scanner.hasNextLine()) {
			String next = scanner.nextLine();
			String[] st = next.split(";");
			logger.info("---- " + next);
			if (st.length < 2 || st[1].indexOf("@") == -1) {
				logger.warning("CSV row error, length or email format: " + next);
				sb.append("CSV row error, length or email format: " + next);
				continue;
			}
			User user = new User();
			user.setCeg(cegId);
			user.setEmail(st[1]);
			// user.setGamegroup(st[0]);
			user.setName(st[0]);
			user.setActive(true);
			user.setRole("USER");
			user.setRegistered(false);

			String usr = getUserRepo().getByField(cegId, "email", "STRING", user.getEmail(), false);

			if (usr.length() > 10) {
				logger.warning("This email is exist: " + user.getEmail());
				sb.append("This email is exist, skipped line: " + next);
				continue;
			}

			logger.info("new user to save: " + gson.toJson(user));

			JsonObject userObj = parser.parse(gson.toJson(user)).getAsJsonObject();

			Identifiable anAggregateRoot = new Identifiable(userObj);

			String id = getUserRepo().save(cegId, anAggregateRoot);

			String email = user.getEmail();
			String name = user.getName();
			String role = user.getRole();
			String mode = "registration";

			String token = sendNepwRegEmail(cegId, mode, email, name, role, id);
		}

		String ret = sb.length() == 0 ? "OK" : sb.toString();
		scanner.close();

		return Response.status(200).entity(ret).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/jwt")
	public Response createJwt(@QueryParam("exp") Long expMsec, AuthClaims auth) {

		String ret = Utils.createJwt(auth, expMsec);

		return Response.status(200).entity(ret).build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/logout")
	// @JWTRoleNeeded({"NONE"})
	@EventLog(value = "USER_LOGOUT")
	public Response logoutUser(@QueryParam("schema") String schema, @CookieParam("x-access-token") String token,
			@QueryParam("no") String no, @Context HttpHeaders headers, String payload) {

//		AuthClaims auth = Utils.checkAuth(token, headers, true);
//		if (auth == null && !"yes".equals(no)) {
//			return Response.status(401).entity("{\"state\": \"there is no valid auth token.\"}").build();
//		}
//		String actUserId = "999999";
//		String userName = "System";
//		if (auth != null) {
//			actUserId = auth.getId();
//			userName = auth.getName();
//		}

		return Response.status(200).entity("OK").build();
	}

	@GET
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users/logoutsessionexpired")
	// @JWTRoleNeeded({"NONE"})
	@EventLog(value = "USER_LOGOUT_SESSIONTIMEOUT")
	public Response logoutSessiontimeoutUser(@QueryParam("schema") String schema,
			@CookieParam("x-access-token") String token, @QueryParam("no") String no, @Context HttpHeaders headers,
			String payload) {

		AuthClaims auth = Utils.checkAuth(token, headers);
		if (auth == null && !"yes".equals(no)) {
			return Response.status(401).entity("{\"state\": \"there is no valid auth token.\"}").build();
		}
		String actUserId = "999999";
		String userName = "System";
		if (auth != null) {
			actUserId = auth.getId();
			userName = auth.getName();
		}

		return Response.status(200).entity("OK").build();
	}

	@GET
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/jwt/{token}")
	public Response resolveJwt(@PathParam("token") String token) {

		boolean valid = Utils.isJwtValid(token);

		AuthClaims auth = null;
		try {
			auth = Utils.getJwtClaims(token);
		} catch (Exception e) {
			return Response.status(500).entity("{\"valid\":" + valid + ",   \"error\": \"" + e.getMessage() + "\"}")
					.build();
		}

		Gson gson = new Gson();

		String json = gson.toJson(auth);

		return Response.status(200).entity("{\"valid\":" + valid + ",   \"auth\": " + json + "}").build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/confirmation/{token}")
	@EventLog(value = "USER_CONFIRM")
	public Response confirm1Registration(@PathParam("cegId") String cegId, @PathParam("token") String token,
			User payload) {

		boolean valid = Utils.isJwtValid(token);

		if (!valid) {
			return createNewToken(token);
		}

		AuthClaims auth = null;
		try {
			auth = Utils.getJwtClaims(token);
		} catch (Exception e) {
			return Response.status(401).entity("{\"status\": \"hib�s\",   \"error\": " + e.getMessage() + "}").build();
		}

		payload.setPassword();

		Identifiable rr = new Identifiable(payload.getJsonObject());
		// Identifiable root = new Identifiable(Long.valueOf(auth.getId()));
		rr.setId(Long.valueOf(auth.getId()));

		boolean success = getUserRepo().setNewPassword(cegId, Long.valueOf(auth.getId()), payload.getHash(),
				payload.getSalt());

		if (success) {
			String message = ("newpw".equals(auth.getSubject())) ? "New password set" : "User registered";
			return Response.status(200).entity("{\"status\": \"OK\",   \"message\": \"" + message + "\"}").build();
		} else {
			return createNewToken(token);
		}
	}

	private Response createNewToken(String token) {
		AuthClaims auth = Utils.getAuth(token);
		String newToken = Utils.createJwt(auth, (long) (30 * 60 * 1000));

		String html = "<!DOCTYPE html>\r\n" + "<html lang=\"en\">\r\n" + "<head>\r\n"
				+ "    <meta charset=\"UTF-8\">\r\n" + "    <title>Title</title>\r\n" + "</head>\r\n" + "<body>";

		String content = "<h3>Game4Team password change</h3>\r\n" + "Please click on this link <a href=";
		String contentEnd = ">password change</a> to change your password.\r\n" + "<br /><br />\r\n"
				+ "Thank you.</body></html>";

		// String clientURL = System.getProperty("CLIENT_URL", "http://localhost:3030");

		String clientURL = System.getenv("CLIENT_URL");

		if (clientURL == null || clientURL.isEmpty()) {
			clientURL = "http://localhost:3030";
		}

		logger.info("createNewToken  -  clientURL (CLIENT_URL): " + clientURL);

		String textMessage = "Your browser does not support html mail. Please click on the following link to change your password: "
				+ clientURL + "/admin/newpw?conf=" + token;
		boolean htmlemail = true;

		SendEmail.sendSimpleEmail(htmlemail, auth.getEmail(), "G4T reset password",
				html + content + clientURL + "/admin/newpw?conf=" + newToken + contentEnd, textMessage, false);

		return Response.status(401).entity("{\"status\": \"Expired\",   \"newToken\": " + newToken + "}").build();
	}

	/**
	 * Felhasználó alap adatok módosítása
	 * 
	 * @param schema
	 * @param tokenForAuth
	 * @param token
	 * @return
	 */
	@PUT
	// @Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cegId}/{userId}/basedata")
	@EventLog(value = "USER_BASEDATA")
	// @JWTRoleNeeded({"ADMIN"})
	public Response updateBaseUserData(@PathParam("cegId") String cegId,
			@HeaderParam("x-access-token") String tokenForAuth, @PathParam("userId") Long userId,
			String payloadUserStr) {

		Identifiable root = new Identifiable(userId);
		String userStr = getUserRepo().getById(cegId, root, NO_LEFTOUT_COLUMN);

		Gson gson = new GsonBuilder().create();
		User origUser = gson.fromJson(userStr, User.class);

		User payloadUser = gson.fromJson(payloadUserStr, User.class);

		String updatedStatus = updateUserBaseData(origUser, payloadUser);

		JsonParser parser = new JsonParser();

		JsonObject updatedUser = parser.parse(gson.toJson(origUser)).getAsJsonObject();

		Identifiable anAggregateRoot = new Identifiable(updatedUser);
		anAggregateRoot.setId(userId);
		getUserRepo().save(cegId, anAggregateRoot);

		return Response.status(200).entity(updatedStatus).build();
	}

	private String updateUserBaseData(User origUser, User payloadUser) {
		JsonArray changed = new JsonArray();
		// name changed
		if (payloadUser.getName() != null && !payloadUser.getName().isEmpty()
				&& !payloadUser.getName().equals(origUser.getName())) {
			changed.add("name " + origUser.getName() + " -> " + payloadUser.getName());
			origUser.setName(payloadUser.getName());
		}

		// role changed
		if (payloadUser.getRole() != null && !payloadUser.getRole().isEmpty()
				&& !payloadUser.getRole().equals(origUser.getRole())) {
			changed.add("name " + origUser.getRole() + " -> " + payloadUser.getRole());
			origUser.setRole(payloadUser.getRole());
		}

		// username changed
		if (payloadUser.getUsername() != null && !payloadUser.getUsername().isEmpty()
				&& !payloadUser.getUsername().equals(origUser.getUsername())) {
			changed.add("name " + origUser.getUsername() + " -> " + payloadUser.getUsername());
			origUser.setUsername(payloadUser.getUsername());
		}

		Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

		return gson.toJson(changed);
	}

}