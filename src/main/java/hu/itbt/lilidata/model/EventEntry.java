package hu.itbt.lilidata.model;

import java.util.ArrayList;

public class EventEntry {
	Data data;
	Schedule schedule;

	// Getter Methods

	public Data getData() {
		return data;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	// Setter Methods

	public void setData(Data dataObject) {
		this.data = dataObject;
	}

	public void setSchedule(Schedule scheduleObject) {
		this.schedule = scheduleObject;
	}

	public static class Schedule {
		ArrayList<String> year = new ArrayList<String>();
		ArrayList<String> month = new ArrayList<String>();
		ArrayList<String> times = new ArrayList<String>();
		private double duration;
		ArrayList<String> dayOfMonth = new ArrayList<String>();
		private String durationUnit;

		// Getter Methods

		public double getDuration() {
			return duration;
		}

		public String getDurationUnit() {
			return durationUnit;
		}

		// Setter Methods

		public void setDuration(double duration) {
			this.duration = duration;
		}

		public void setDurationUnit(String durationUnit) {
			this.durationUnit = durationUnit;
		}

		public void addYear(String ayear) {
			year.add(ayear);
		}

		public void addMonth(String amonth) {
			month.add(amonth);
		}

		public void addDayOfMonth(String aday) {
			dayOfMonth.add(aday);
		}

		public ArrayList<String> getYear() {
			return year;
		}

		public void setYear(ArrayList<String> year) {
			this.year = year;
		}

		public ArrayList<String> getMonth() {
			return month;
		}

		public void setMonth(ArrayList<String> month) {
			this.month = month;
		}

		public ArrayList<String> getTimes() {
			return times;
		}

		public void setTimes(ArrayList<String> times) {
			this.times = times;
		}

		public ArrayList<String> getDayOfMonth() {
			return dayOfMonth;
		}

		public void setDayOfMonth(ArrayList<String> dayOfMonth) {
			this.dayOfMonth = dayOfMonth;
		}

		@Override
		public String toString() {
			return "Schedule [year=" + year + ", month=" + month + ", times=" + times + ", duration=" + duration
					+ ", dayOfMonth=" + dayOfMonth + ", durationUnit=" + durationUnit + "]";
		}

	}

	public static class Data {
		private float id;
		private String title;
		private String color;
		private String description = null;
		private String forecolor;
		private String icon = null;
		private String location;
		private String user_id;
		private String start;
		private String end;
		private String how = null;
		private float point;
		private String sport = null;
		private String activity = null;
		private String invite = null;
		private float finished;
		private String created_at;
		private String updated_at;
		Comments CommentsObject;

		// Getter Methods

		public float getId() {
			return id;
		}

		public String getTitle() {
			return title;
		}

		public String getColor() {
			return color;
		}

		public String getDescription() {
			return description;
		}

		public String getForecolor() {
			return forecolor;
		}

		public String getIcon() {
			return icon;
		}

		public String getLocation() {
			return location;
		}

		public String getUser_id() {
			return user_id;
		}

		public String getStart() {
			return start;
		}

		public String getEnd() {
			return end;
		}

		public String getHow() {
			return how;
		}

		public float getPoint() {
			return point;
		}

		public String getSport() {
			return sport;
		}

		public String getActivity() {
			return activity;
		}

		public String getInvite() {
			return invite;
		}

		public float getFinished() {
			return finished;
		}

		public String getCreated_at() {
			return created_at;
		}

		public String getUpdated_at() {
			return updated_at;
		}

		public Comments getComments() {
			return CommentsObject;
		}

		// Setter Methods

		public void setId(float id) {
			this.id = id;
		}

		public void setTitle(String title) {
			this.title = title;
		}

		public void setColor(String color) {
			this.color = color;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		public void setForecolor(String forecolor) {
			this.forecolor = forecolor;
		}

		public void setIcon(String icon) {
			this.icon = icon;
		}

		public void setLocation(String location) {
			this.location = location;
		}

		public void setUser_id(String user_id) {
			this.user_id = user_id;
		}

		public void setStart(String start) {
			this.start = start;
		}

		public void setEnd(String end) {
			this.end = end;
		}

		public void setHow(String how) {
			this.how = how;
		}

		public void setPoint(float point) {
			this.point = point;
		}

		public void setSport(String sport) {
			this.sport = sport;
		}

		public void setActivity(String activity) {
			this.activity = activity;
		}

		public void setInvite(String invite) {
			this.invite = invite;
		}

		public void setFinished(float finished) {
			this.finished = finished;
		}

		public void setCreated_at(String created_at) {
			this.created_at = created_at;
		}

		public void setUpdated_at(String updated_at) {
			this.updated_at = updated_at;
		}

		public void setComments(Comments commentsObject) {
			this.CommentsObject = commentsObject;
		}

		@Override
		public String toString() {
			return "Data [id=" + id + ", title=" + title + ", color=" + color + ", description=" + description
					+ ", forecolor=" + forecolor + ", icon=" + icon + ", location=" + location + ", user_id=" + user_id
					+ ", start=" + start + ", end=" + end + ", how=" + how + ", point=" + point + ", sport=" + sport
					+ ", activity=" + activity + ", invite=" + invite + ", finished=" + finished + ", created_at="
					+ created_at + ", updated_at=" + updated_at + ", CommentsObject=" + CommentsObject + "]";
		}

	}

	public static class Comments {
		private String date;
		private String time;
		private String how = null;
		ArrayList<String> comments = new ArrayList<String>();

		// Getter Methods

		public String getDate() {
			return date;
		}

		public String getTime() {
			return time;
		}

		public String getHow() {
			return how;
		}

		// Setter Methods

		public void setDate(String date) {
			this.date = date;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public void setHow(String how) {
			this.how = how;
		}

		@Override
		public String toString() {
			return "Comments [date=" + date + ", time=" + time + ", how=" + how + ", comments=" + comments + "]";
		}
	}
}