package hu.itbt.lilidata.model;

public class Participant {
	protected long id;
	protected String disp;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDisp() {
		return disp;
	}

	public void setDisp(String disp) {
		this.disp = disp;
	}

	@Override
	public String toString() {
		return "Location [id=" + id + ", disp=" + disp + "]";
	}

}
