package hu.itbt.lilidata.model;

import java.time.LocalDateTime;

public class CalendarEvent {
	protected long id;

	transient protected LocalDateTime created_at;
	transient protected long created_by;
	transient protected LocalDateTime modified_at;
	transient protected long modified_by;
	protected String event_type;
	protected String event_category;
	protected String time;
	protected Event event;
	protected String day;

	protected long clientid;
	protected long coachid;

	public long getClientid() {
		return clientid;
	}

	public void setClientid(long clientid) {
		this.clientid = clientid;
	}

	public long getCoachid() {
		return coachid;
	}

	public void setCoachid(long coachid) {
		this.coachid = coachid;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public LocalDateTime getCreated_at() {
		return created_at;
	}

	public void setCreated_at(LocalDateTime created_at) {
		this.created_at = created_at;
	}

	public long getCreated_by() {
		return created_by;
	}

	public void setCreated_by(long created_by) {
		this.created_by = created_by;
	}

	public LocalDateTime getModified_at() {
		return modified_at;
	}

	public void setModified_at(LocalDateTime modified_at) {
		this.modified_at = modified_at;
	}

	public long getModified_by() {
		return modified_by;
	}

	public void setModified_by(long modified_by) {
		this.modified_by = modified_by;
	}

	public String getEvent_type() {
		return event_type;
	}

	public void setEvent_type(String event_type) {
		this.event_type = event_type;
	}

	public String getEvent_category() {
		return event_category;
	}

	public void setEvent_category(String event_category) {
		this.event_category = event_category;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@Override
	public String toString() {
		return "CalendarEvent [id=" + id + ", event_type=" + event_type + ", event_category=" + event_category
				+ ", time=" + time + ", event=" + event + ", day=" + day + ", clientid=" + clientid + ", coachid="
				+ coachid + "]";
	}

}
