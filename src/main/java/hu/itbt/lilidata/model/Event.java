package hu.itbt.lilidata.model;

import java.util.List;

public class Event {

	private long id;
	private String end;
	private String name;
	private Alert alert;
	private List<Participant> participants;
	private Location location;
	private String color;
	private String start;
	private Boolean timed;
	private int points;
	private String details;
	private List<Comment> comments;
	private String category;
	private int progress;
	private boolean done;

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public void setProgress(int progress) {
		this.progress = progress;
	}

	public List<Participant> getParticipants() {
		return participants;
	}

	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public long getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Alert getAlert() {
		return alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public Boolean getTimed() {
		return timed;
	}

	public void setTimed(Boolean timed) {
		this.timed = timed;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getProgress() {
		return progress;
	}

	public void setProgress(Integer progress) {
		this.progress = progress;
	}

	@Override
	public String toString() {
		return "Event [id=" + id + ", end=" + end + ", name=" + name + ", alert=" + alert + ", participants="
				+ participants + ", location=" + location + ", color=" + color + ", start=" + start + ", timed=" + timed
				+ ", points=" + points + ", details=" + details + ", category=" + category + ", progress=" + progress
				+ ", done=" + done + "]";
	}

	public static class Comment {
		protected String id;
		protected String text;
		protected String avatar;
		protected String time;
		protected String ccsclass;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public String getAvatar() {
			return avatar;
		}

		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}

		public String getTime() {
			return time;
		}

		public void setTime(String time) {
			this.time = time;
		}

		public String getCcsclass() {
			return ccsclass;
		}

		public void setCcsclass(String ccsclass) {
			this.ccsclass = ccsclass;
		}

		@Override
		public String toString() {
			return "Comment [id=" + id + ", text=" + text + ", avatar=" + avatar + ", time=" + time + ", ccsclass="
					+ ccsclass + "]";
		}

	}
}