package hu.itbt.lilidata.model;

public class Alert {
	protected String text;
	protected String value;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Alert [text=" + text + ",  value=" + value + "]";
	}

}
