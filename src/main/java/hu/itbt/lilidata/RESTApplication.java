package hu.itbt.lilidata;
// import the rest service you created!

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import hu.itbt.lilidata.pages.IndexPage;
import hu.itbt.lilidata.rest.AdminRest;
import hu.itbt.lilidata.rest.EventsRest;
import hu.itbt.lilidata.rest.ImagesRest;
import hu.itbt.lilidata.rest.QuestionsRest;
import hu.itbt.lilidata.rest.SettingsRest;
import hu.itbt.lilidata.rest.UsersRest;
import hu.itbt.lilidata.utils.CORSFilter;
import hu.itbt.lilidata.utils.JWTRoleNeededFilter;

@ApplicationPath("/rest")
public class RESTApplication extends Application {

	public RESTApplication() {
	}

	/**
	 * Gets the classes.
	 *
	 * @return the classes
	 */
	@Override
	public Set<Class<?>> getClasses() {
		final Set<Class<?>> classes = new HashSet<>();

		classes.add(JWTRoleNeededFilter.class);
		classes.add(CORSFilter.class);

		classes.add(UsersRest.class);
		classes.add(EventsRest.class);
		classes.add(ImagesRest.class);
		classes.add(QuestionsRest.class);
		classes.add(SettingsRest.class);
		classes.add(AdminRest.class);

		classes.add(IndexPage.class);

		return classes;
	}
}